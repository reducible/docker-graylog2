# Start/stop a basic Graylog2 server using docker-compose

Consists of 3x containers:

- elasticsearch
- mongodb
- graylog2

See `docker-compose.yaml` for details.

## Start

```
./start.sh
```

## Stop

```
./stop.sh
```

Example taken from: https://hub.docker.com/r/graylog2/server/
